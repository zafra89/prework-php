<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP prework ejercicio 1</title>
</head>
<body>
  <?php
      function dayOfTheWeek($num) {
        if ($num == 1) {
          echo "Lunes";
        } else if ($num == 2) {
          echo "Martes";
        } else if ($num == 3) {
          echo "Miércoles";
        } else if ($num == 4) {
          echo "Jueves";
        } else if ($num == 5) {
          echo "Viernes";
        } else if ($num == 6) {
          echo "Sábado";
        } else if ($num == 7) {
          echo "Domingo";
        } else {
          echo "El número debe ser entre 1 y 7";
        }
      }
    ?>
</body>
</html>