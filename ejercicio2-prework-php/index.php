<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP prework ejercicio 2</title>
</head>
<body>
  <?php
    function hasAllVowels($str) {
      $vowels = array("a", "e", "i", "o", "u");
      for ($i = 0; $i < strlen($str); $i++) {
        if (array_search($str[$i], $vowels) > -1) {
          array_splice($vowels, array_search($str[$i], $vowels), 1);
        }
      }
      if (count($vowels) > 0) {
        echo "NO CONTIENE TODAS LAS VOCALES";
      } else if (count($vowels) == 0) {
        echo "LA CADENA CONTIENE LAS 5 VOCALES";
      }
    }
  ?>
</body>
</html>