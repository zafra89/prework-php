<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP prework ejercicio 4</title>
</head>
<body>
  <?php
    function replace($fileName, $copiedFile, $wordToReplace, $replacement) {
      copy($fileName, $copiedFile);
      $content = file_get_contents($copiedFile);
      $replace = str_replace($wordToReplace, $replacement, $content);
      file_put_contents($copiedFile, $replace);
    }

    replace('quijote.txt', 'quijote-modificado.txt', 'Sancho', 'Morty');
  ?>
</body>
</html>